/**
 * 
 */
package com.caversia.plugins.economy.listeners;

import java.util.Optional;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import com.caversia.plugins.economy.model.CustomItem;
import com.caversia.plugins.economy.persistence.ItemsRepository;

/**
 * Listens for {@link BlockBreakEvent} and if it's a buy/sell sign then remove the reference from our custom items list.
 * 
 * @author amartins
 */
public class SignBreakListener implements Listener {

    /**
     * Handles the event.
     * 
     * @param evt the event
     */
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onChange(final BlockBreakEvent evt) {
        BlockState blockState = evt.getBlock().getState();
        if (!(blockState instanceof Sign)) {
            return;
        }

        Sign sign = (Sign) blockState;
        String title = sign.getLine(0);
        if (!title.equals(ChatColor.DARK_BLUE + "[cBuy]") && !title.equals(ChatColor.DARK_BLUE + "[cSell]")) {
            return;
        }

        if (!evt.getPlayer().isOp()) {
            evt.setCancelled(true);
            return;
        }

        String itemName = sign.getLine(2);
        Optional<CustomItem> item = ItemsRepository.INSTANCE.getItem(itemName);
        if (!item.isPresent()) {
            return;
        }

        item.get().removeSignReference(Bukkit.getServerName(), evt.getBlock());
    }
}
