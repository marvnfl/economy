/**
 * 
 */
package com.caversia.plugins.economy.listeners;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.caversia.plugins.economy.gui.ItemsStoreGUI;

/**
 * Listens to events of type {@link InventoryClickEvent} and handles players interaction with {@link ItemsStoreGUI}.
 * 
 * @author amartins
 */
public class ItemsStoreClickListener implements Listener {

    /**
     * Handles the click on a {@link ItemsStoreGUI} window.
     * 
     * @param evt the {@link InventoryClickEvent}
     */
    @EventHandler
    public void onClick(final InventoryClickEvent evt) {
        InventoryHolder holder = evt.getInventory().getHolder();
        if (!(holder instanceof ItemsStoreGUI)) {
            return;
        }

        ItemStack item = evt.getCurrentItem();
        if (item == null || Material.AIR.equals(item.getType())) {
            return;
        }

        item = item.clone();
        ItemMeta itemMeta = item.getItemMeta();
        List<String> lore = itemMeta.getLore();
        if (lore != null && lore.size() >= 2) {
            itemMeta.setLore(lore.subList(0, lore.size() - 2));
        } else {
            itemMeta.setLore(null);
        }
        item.setItemMeta(itemMeta);

        ItemsStoreGUI store = (ItemsStoreGUI) holder;
        store.onClickEvent((Player) evt.getWhoClicked(), item, evt.getRawSlot());

        evt.setCancelled(true);
    }
}
