package com.caversia.plugins.economy.listeners;

import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

import com.caversia.plugins.economy.persistence.ItemsRepository;
import com.caversia.plugins.economy.utils.ChatMessage;
import com.caversia.plugins.economy.utils.Config;
import com.caversia.plugins.economy.utils.SignsUtils;
import com.caversia.plugins.economy.utils.exceptions.InvalidSignDataException;

/**
 * Listens to events of type {@link SignChangeEvent} and handles Caversia buy/sell signs creation.
 * 
 * @author amartins
 */
public class SignChangeListener implements Listener {

    /**
     * Handles the {@link SignChangeEvent}.
     * 
     * @param evt the change event
     */
    @EventHandler
    public void onChange(final SignChangeEvent evt) {
        if (!evt.getPlayer().isOp()) {
            return;
        }

        String line0 = evt.getLine(0).toLowerCase();
        if (!line0.contains("cbuy") && !line0.contains("csell")) {
            return;
        }

        try {
            validateSignData(evt.getLines());
        } catch (InvalidSignDataException isde) {
            evt.getPlayer().sendMessage(ChatColor.RED + isde.getMessage());
            return;
        }

        //@formatter:off
        ItemsRepository.INSTANCE.getItem(evt.getLine(2))
                                .get()
                                .addSignReference(Bukkit.getServerName(), evt.getBlock());
        //@formatter:off
        
        // Colorify the title
        String signTitle = line0.contains("cbuy") ? "[cBuy]" : "[cSell]";
        evt.setLine(0, ChatColor.DARK_BLUE + signTitle);

        // Make sure that the currency symbol appears in front
        evt.setLine(3, Config.getCurrencySymbol() + String.format("%.2f",SignsUtils.getItemPrice(evt.getLine(3))));

      //@formatter:off
        ChatMessage.to(evt.getPlayer())
                   .yellow("You successfully created a ")
                   .darkBlue(signTitle).yellow(" sign for item ").darkPurple(evt.getLine(2))
                   .send();
      //@formatter:off
    }

    /**
     * Validates the info entered on buy/sell signs.
     * 
     * @param lines the lines of the sign to validate
     * @throws InvalidSignDataException when invalid data exists on the provided lines
     */
    private void validateSignData(String[] lines) throws InvalidSignDataException {

        if (!NumberUtils.isNumber(lines[1])) {
            throw new InvalidSignDataException("Line 2 of the sign must contain a valid number.");
        }

        if (!ItemsRepository.INSTANCE.getItem(lines[2]).isPresent()) {
            throw new InvalidSignDataException("Line 3 of the sign must contain the name of an existing custom item.");
        }

        try {
            SignsUtils.getItemPrice(lines[3]);
        } catch (Exception e) {
            throw new InvalidSignDataException("Line 4 of the sign must contain a valid price.");
        }
    }
}
