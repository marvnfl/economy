/**
 * 
 */
package com.caversia.plugins.economy.listeners;

import java.util.Optional;

import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.caversia.plugins.economy.gui.BuyItemsStore;
import com.caversia.plugins.economy.gui.SellItemsStore;
import com.caversia.plugins.economy.model.CustomItem;
import com.caversia.plugins.economy.persistence.ItemsRepository;
import com.caversia.plugins.economy.utils.ChatMessage;
import com.caversia.plugins.economy.utils.InventoryUtils;
import com.caversia.plugins.economy.utils.Logger;
import com.caversia.plugins.economy.utils.SignsUtils;
import com.caversia.plugins.economy.utils.SignsUtils.Type;

/**
 * @author amartins
 */
public class SignInteractListener implements Listener {

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onInteraction(final PlayerInteractEvent evt) {
        Block block = evt.getClickedBlock();
        if (block == null)
            return;

        if (!Action.RIGHT_CLICK_BLOCK.equals(evt.getAction())) {
            return;
        }

        if (!(block.getState() instanceof Sign))
            return;

        Sign sign = (Sign) block.getState();
        if (!SignsUtils.isBuySign(sign) && !SignsUtils.isSellSign(sign)) {
            return;
        }

        Optional<CustomItem> item = ItemsRepository.INSTANCE.getItem(sign.getLine(2));
        if (!item.isPresent()) {
            Logger.error("Sign at <{}> refers a non existing item <{}>", sign.getLocation(), sign.getLine(2));
            return;
        }

        Type signType = SignsUtils.getSignType(sign);
        ItemStack itemStack = item.get().asItemStack(1);

        if (Type.BUY.equals(signType)) {
            // http://198.27.67.155:8112/issue/CE-10
            if (itemStack.getMaxStackSize() == 1) {
                BuyItemsStore store = new BuyItemsStore(item.get(), SignsUtils.getItemPrice(sign));
                store.onClickEvent(evt.getPlayer(), itemStack);
            } else {
                new BuyItemsStore(evt.getPlayer(), item.get(), SignsUtils.getItemPrice(sign));
            }
            return;
        }

        switch (InventoryUtils.countItems(evt.getPlayer().getInventory(), itemStack)) {
        case 0:
            ChatMessage.to(evt.getPlayer()).red("You don't have items of this type to sell.").send();
            break;
        case 1:
            SellItemsStore store = new SellItemsStore(item.get(), SignsUtils.getItemPrice(sign));
            store.onClickEvent(evt.getPlayer(), itemStack);
            break;
        default:
            new SellItemsStore(evt.getPlayer(), item.get(), SignsUtils.getItemPrice(sign));
        }
    }
}
