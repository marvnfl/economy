package com.caversia.plugins.economy;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.caversia.plugins.economy.commands.RootCommands;
import com.caversia.plugins.economy.listeners.ItemsStoreClickListener;
import com.caversia.plugins.economy.listeners.SignBreakListener;
import com.caversia.plugins.economy.listeners.SignChangeListener;
import com.caversia.plugins.economy.listeners.SignInteractListener;
import com.caversia.plugins.economy.persistence.ItemsRepository;
import com.caversia.plugins.economy.utils.Config;
import com.caversia.plugins.economy.utils.Logger;
import com.caversia.plugins.economy.utils.Plugins;
import com.caversia.plugins.economy.utils.exceptions.EconomySetupFailedException;
import com.sk89q.bukkit.util.CommandsManagerRegistration;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissionsException;
import com.sk89q.minecraft.util.commands.CommandUsageException;
import com.sk89q.minecraft.util.commands.CommandsManager;
import com.sk89q.minecraft.util.commands.MissingNestedCommandException;
import com.sk89q.minecraft.util.commands.SimpleInjector;
import com.sk89q.minecraft.util.commands.WrappedCommandException;

/**
 * Plugin entry point.
 * 
 * @author sp3c
 * @author marvnfl
 */
public class Main extends JavaPlugin implements TabCompleter {
    public static Main self;
    private CommandsManager<CommandSender> commandsManager;

    @Override
    public void onEnable() {
        try {
            Config.initialize(this);
            Logger.initialize(getLogger());
            Main.self = this;

            setupEconomy();
            setupCommands();
            setupListeners();
            setupPersistence();

        } catch (EconomySetupFailedException e) {
            setEnabled(false);
            Logger.error("Caversia Economy Plugin failed to start.");
            return;
        }

        Logger.info("Caversia Economy Plugin was enabled.");
    }

    @Override
    public void onDisable() {
        Logger.info("Caversia Economy Plugin was disabled.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

        try {
            this.commandsManager.execute(cmd.getName(), args, sender, sender);
        } catch (CommandPermissionsException e) {
            sender.sendMessage(ChatColor.RED + "You don't have permission.");
        } catch (MissingNestedCommandException e) {
            sender.sendMessage(ChatColor.RED + e.getUsage());
        } catch (CommandUsageException e) {
            sender.sendMessage(ChatColor.RED + e.getMessage());
            sender.sendMessage(ChatColor.RED + e.getUsage());
        } catch (WrappedCommandException e) {
            if (e.getCause() instanceof NumberFormatException) {
                sender.sendMessage(ChatColor.RED + "Number expected, string received instead.");
            } else {
                sender.sendMessage(ChatColor.RED + "An error has occurred. See console.");
                e.printStackTrace();
            }
        } catch (CommandException e) {
            sender.sendMessage(ChatColor.RED + e.getMessage());
        }

        return true;
    }

    /**
     * Loads all the configuration files.
     */
    public void reloadAll() {
        Config.reload();
        ItemsRepository.INSTANCE.loadAll();
    }

    /**
     * Saves all the information currently in memory to disk configuration files.
     */
    public void saveAll() {
        ItemsRepository.INSTANCE.saveAll();
    }

    private void setupListeners() {
        getServer().getPluginManager().registerEvents(new SignChangeListener(), this);
        getServer().getPluginManager().registerEvents(new SignBreakListener(), this);
        getServer().getPluginManager().registerEvents(new SignInteractListener(), this);
        getServer().getPluginManager().registerEvents(new ItemsStoreClickListener(), this);
    }

    /**
     * Sets up everything related with persistence.
     */
    private void setupPersistence() {
        reloadAll();
    }

    /**
     * Sets up everything related with the plugin commands.
     */
    private void setupCommands() {
        this.commandsManager = new CommandsManager<CommandSender>() {
            @Override
            public boolean hasPermission(CommandSender sender, String permission) {
                return sender.hasPermission(permission);
            }
        };
        commandsManager.setInjector(new SimpleInjector(this));
        CommandsManagerRegistration registration = new CommandsManagerRegistration(this, commandsManager);
        registration.register(RootCommands.class);
    }

    private void setupEconomy() throws EconomySetupFailedException {
        Plugin plugin = getServer().getPluginManager().getPlugin("Vault");
        if (plugin == null) {
            Logger.error("Vault dependency missing.");
            throw new EconomySetupFailedException();
        }

        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        Economy economy = rsp.getProvider();
        if (economy == null) {
            Logger.error("Failed to register economy.");
            throw new EconomySetupFailedException();
        }

        Plugins.economy = economy;
    }
}
