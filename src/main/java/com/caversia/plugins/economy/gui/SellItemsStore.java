/**
 * 
 */
package com.caversia.plugins.economy.gui;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.caversia.plugins.economy.model.CustomItem;
import com.caversia.plugins.economy.persistence.ItemsRepository;
import com.caversia.plugins.economy.utils.ChatMessage;
import com.caversia.plugins.economy.utils.Config;
import com.caversia.plugins.economy.utils.InventoryUtils;
import com.caversia.plugins.economy.utils.Plugins;

/**
 * Implementation of a sell store.
 * 
 * @author amartins
 */
public class SellItemsStore extends ItemsStoreGUI {

    /**
     * Constructor.
     * 
     * @param player the player to which the store is to be shown
     * @param item the item that the store will work with
     * @param pricePerUnit the price per unit of the item provided
     */
    public SellItemsStore(Player player, CustomItem item, float pricePerUnit) {
        super(player, item, pricePerUnit);
    }

    /**
     * Constructor.
     * 
     * @param player the player to which the store is to be shown
     * @param item the item that the store will work with
     * @param pricePerUnit the price per unit of the item provided
     */
    public SellItemsStore(CustomItem item, float pricePerUnit) {
        super(null, item, pricePerUnit);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.caversia.plugins.economy.gui.ItemsStoreGUI#onClickEvent(org.bukkit.event.inventory.InventoryClickEvent)
     */
    @Override
    public void onClickEvent(Player player, ItemStack clickedOption, int slotIdx) {

        int ownedItemsQty = InventoryUtils.countItems(player.getInventory(), clickedOption);
        if (ownedItemsQty == 0) {
            ChatMessage.to(player).red("You don't have any more items to sell.").send();
            return;
        }

        // Option at idx 5 on the inventory is the "Sell All"
        int qtySold = (slotIdx == 5) ? ownedItemsQty : clickedOption.getAmount();
        if (ownedItemsQty < qtySold) {
            ChatMessage.to(player).red("You don't have that amount of items to sell.").send();
            return;
        }

        InventoryUtils.takeItems(player.getInventory(), itemName, qtySold);
        player.updateInventory();

        float totalProfit = qtySold * pricePerUnit;
        Plugins.economy.depositPlayer(player, totalProfit);

        //@formatter:off
        ChatMessage.to(player)
                   .yellow("You successfully sold ").lPurple(qtySold, 'x', itemName)
                   .yellow(" for ").green(Config.getCurrencySymbol(), String.format("%.2f",totalProfit))
                   .send();
        //@formatter:on

        if (!setupOptions(player, ItemsRepository.INSTANCE.getItem(itemName).get())) {
            player.closeInventory();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.caversia.plugins.economy.gui.ItemsStoreGUI#setupOptions(org.bukkit.entity.Player,
     * com.caversia.plugins.economy.model.CustomItem)
     */
    @Override
    protected boolean setupOptions(Player player, CustomItem item) {

        ItemStack oneItemStack = item.asItemStack(1);
        int playerItemsCount = InventoryUtils.countItems(player.getInventory(), oneItemStack);
        if (playerItemsCount == 0) {
            return false;
        }

        // Clear any already existing options
        inventory.clear();
        inventory.setItem(3, addSellLore(oneItemStack, 1));

        int maxStackSize = oneItemStack.getMaxStackSize();
        if (maxStackSize > 1) {
            // Only present the full stack option when the item is stackable
            // http://198.27.67.155:8112/issue/CE-10
            inventory.setItem(4, addSellLore(item.asItemStack(maxStackSize), maxStackSize));
        }

        if (playerItemsCount > 1) {
            inventory.setItem(5, addSellLore(item.asItemStack(1), playerItemsCount));
        }

        return true;
    }

    /**
     * Adds the store sell info to the lore of an item used as a option.
     * 
     * @param option the option to add the info to
     * @param count the number of items that will be sell with this option
     * @return the option with the info already added
     */
    private ItemStack addSellLore(ItemStack option, int count) {
        ItemMeta itemMeta = option.getItemMeta();
        List<String> lore = itemMeta.getLore();
        if (lore == null) {
            lore = new ArrayList<>(2);
        }
        lore.add("");

        StringBuilder sb = new StringBuilder();
        sb.append(ChatColor.WHITE).append("Sell ");
        sb.append(ChatColor.YELLOW).append(count).append(count > 1 ? " items" : " item");
        sb.append(ChatColor.WHITE).append(" for ");
        sb.append(ChatColor.DARK_GREEN).append(Config.getCurrencySymbol())
                .append(String.format("%.2f", count * pricePerUnit));

        lore.add(sb.toString());
        itemMeta.setLore(lore);
        option.setItemMeta(itemMeta);

        return option;
    }
}
