/**
 * 
 */
package com.caversia.plugins.economy.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.caversia.plugins.economy.model.CustomItem;
import com.caversia.plugins.economy.persistence.ItemsRepository;
import com.caversia.plugins.economy.utils.ChatMessage;
import com.caversia.plugins.economy.utils.Config;
import com.caversia.plugins.economy.utils.Plugins;

/**
 * Implementation of a buy store.
 * 
 * @author amartins
 */
public class BuyItemsStore extends ItemsStoreGUI {

    /**
     * Constructor.
     * 
     * @param player the player to which the store is to be shown
     * @param item the item that the store will work with
     * @param pricePerUnit the price per unit of the item provided
     */
    public BuyItemsStore(Player player, CustomItem item, float pricePerUnit) {
        super(player, item, pricePerUnit);
    }

    /**
     * Constructor.
     * 
     * @param item the item that the store will work with
     * @param pricePerUnit the price per unit of the item provided
     */
    public BuyItemsStore(CustomItem item, float pricePerUnit) {
        super(null, item, pricePerUnit);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.caversia.plugins.economy.gui.ItemsStoreGUI#onClickEvent(org.bukkit.event.inventory.InventoryClickEvent)
     */
    @Override
    public void onClickEvent(Player player, ItemStack clickedOption, int slotIdx) {

        int qtyBought = clickedOption.getAmount();
        float totalCost = qtyBought * pricePerUnit;
        if (!Plugins.economy.has(player, totalCost)) {
            ChatMessage.to(player).red("You don't have enough money to buy this item.").send();
            return;
        }

        CustomItem customItem = ItemsRepository.INSTANCE.getItem(itemName).get();
        ItemStack itemStack = customItem.asItemStack(qtyBought);
        HashMap<Integer, ItemStack> scraps = player.getInventory().addItem(itemStack);
        for (ItemStack scrap : scraps.values()) {
            totalCost -= scrap.getAmount() * pricePerUnit;
            qtyBought -= scrap.getAmount();
        }
        player.updateInventory();

        if (qtyBought == 0) {
            ChatMessage.to(player).red("Your inventory is full! You cannot buy this item!").send();
            return;
        }

        Plugins.economy.withdrawPlayer(player, totalCost);

        //@formatter:off
        ChatMessage.to(player)
                   .yellow("You successfully bought ").lPurple(qtyBought, "x", itemName)
                   .yellow(" for ").green(Config.getCurrencySymbol(), String.format("%.2f",totalCost))
                   .send();
        //@formatter:on
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.caversia.plugins.economy.gui.ItemsStoreGUI#setupOptions(org.bukkit.entity.Player,
     * com.caversia.plugins.economy.model.CustomItem)
     */
    @Override
    protected boolean setupOptions(Player player, CustomItem item) {
        ItemStack oneItemStack = item.asItemStack(1);
        inventory.setItem(3, addBuyLore(player, oneItemStack));
        inventory.setItem(4, addBuyLore(player, item.asItemStack(oneItemStack.getMaxStackSize() / 2)));
        inventory.setItem(5, addBuyLore(player, item.asItemStack(oneItemStack.getMaxStackSize())));

        return true;
    }

    /**
     * Adds the store buy info to the lore of an item used as a option.
     * 
     * @param player the player that will interact with the store
     * @param option the option to add the info to
     * @return the item with the info already added
     */
    private ItemStack addBuyLore(Player player, ItemStack option) {
        ItemMeta itemMeta = option.getItemMeta();
        List<String> lore = itemMeta.getLore();
        if (lore == null) {
            lore = new ArrayList<>(2);
        }

        lore.add("");

        float totalCost = option.getAmount() * pricePerUnit;
        ChatColor priceColor = Plugins.economy.has(player, totalCost) ? ChatColor.DARK_GREEN : ChatColor.RED;

        StringBuilder sb = new StringBuilder();
        sb.append(ChatColor.WHITE).append("Buy ");
        sb.append(ChatColor.YELLOW).append(option.getAmount()).append(option.getAmount() > 1 ? " items" : " item");
        sb.append(ChatColor.WHITE).append(" for ");
        sb.append(priceColor).append(Config.getCurrencySymbol()).append(String.format("%.2f", totalCost));

        lore.add(sb.toString());
        itemMeta.setLore(lore);
        option.setItemMeta(itemMeta);

        return option;
    }
}
