package com.caversia.plugins.economy.gui;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import com.caversia.plugins.economy.model.CustomItem;

/**
 * Represents a store GUI that will allow a player to sell/buy custom items.
 * 
 * @author amartins
 */
public abstract class ItemsStoreGUI implements InventoryHolder {

    protected String itemName;
    
    protected float pricePerUnit;
    
    protected Inventory inventory;

    /**
     * Constructor.
     * 
     * @param player the player to which the store is to be shown.
     * @param item the item that the store will work with
     * @param pricePerUnit the price per unit of the item provided
     */
    public ItemsStoreGUI(Player player, CustomItem item, float pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
        this.itemName = item.getName();

        if (player == null) {
            return;
        }

        this.inventory = Bukkit.createInventory(this, 9, ChatColor.DARK_BLUE + "Caversia Store");
        setupOptions(player, item);
        player.openInventory(inventory);
    }

    /**
     * Handles a click from the player on the inventory.
     * 
     * @param player the player
     * @param clickedOption the option that the player clicked
     */
    public void onClickEvent(Player player, ItemStack clickedOption) {
        onClickEvent(player, clickedOption, -1);
    }

    /**
     * Handles a click from the player on the inventory.
     * 
     * @param player the player
     * @param clickedOption the option that the player clicked
     * @param slotIdx the index of the slot clicked
     */
    public abstract void onClickEvent(Player player, ItemStack clickedOption, int slotIdx);

    /**
     * Sets up an the inventory options to show to the player.
     * 
     * @param player the player that will interact with the store
     * @param item the item that will be used as options for the player to interact with
     */
    protected abstract boolean setupOptions(Player player, CustomItem item);

    /*
     * (non-Javadoc)
     * 
     * @see org.bukkit.inventory.InventoryHolder#getInventory()
     */
    @Override
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * Gets the itemName.
     *
     * @return the itemName
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * Gets the pricePerUnit.
     *
     * @return the pricePerUnit
     */
    public float getPricePerUnit() {
        return pricePerUnit;
    }
}
