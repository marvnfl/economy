/**
 * 
 */
package com.caversia.plugins.economy.persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 * @author amartins
 */
public enum PersistenceManager {
    INSTANCE;

    private SessionFactory sessionsFactory;

    private PersistenceManager() {
        Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
        Configuration config = new Configuration().configure();
        sessionsFactory = config.buildSessionFactory(new StandardServiceRegistryBuilder().configure().build());
    }

    public Session getSession() {
        return sessionsFactory.openSession();
    }

    public void close() {
        sessionsFactory.close();
    }
}
