/**
 * 
 */
package com.caversia.plugins.economy.persistence;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.caversia.plugins.economy.Main;
import com.caversia.plugins.economy.model.CustomItem;
import com.caversia.plugins.economy.utils.Logger;
import com.caversia.plugins.economy.utils.exceptions.ItemAlreadyExistsException;
import com.caversia.plugins.economy.utils.exceptions.ItemReferencedBySignException;
import com.google.common.collect.ImmutableList;

/**
 * Implementation of the custom items repository using the Singleton pattern.
 * 
 * @author sp3c
 */
public enum ItemsRepository {

    INSTANCE;

    private static final Boolean AUTO_SAVE = Boolean.TRUE;

    private Map<String, CustomItem> items = new ConcurrentHashMap<>();

    /**
     * Marks the repository as being dirty.
     */
    public void setChanged() {
        if (AUTO_SAVE) {
            Bukkit.getScheduler().runTaskAsynchronously(Main.self, () -> saveAll());
        }
    }

    /**
     * Adds a new {@link CustomItem} to the repository.
     * 
     * @param item the item to add
     * @throws ItemAlreadyExistsException when an item with the same name already exists on the repository
     */
    public void addItem(CustomItem item) throws ItemAlreadyExistsException {
        if (items.containsKey(item.getName())) {
            throw new ItemAlreadyExistsException();
        }

        items.put(item.getName().toUpperCase(), item);

        Session session = PersistenceManager.INSTANCE.getSession();
        try {
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(item);
            transaction.commit();
        } finally {
            session.close();
        }
    }

    /**
     * Removes an item from this repository.
     * 
     * @param name the name of the item to remove
     * @return the removed item.
     * @throws ItemReferencedBySignException when the item being removed is still being referenced by buy/sell signs
     */
    public Optional<CustomItem> remove(String name) throws ItemReferencedBySignException {
        CustomItem item = items.get(name.toUpperCase());
        if (item != null && item.isReferencedBySigns()) {
            throw new ItemReferencedBySignException();
        }

        CustomItem removedItem = items.remove(name.toUpperCase());
        if (removedItem != null) {
            Session session = PersistenceManager.INSTANCE.getSession();
            Transaction transaction = session.beginTransaction();
            try {
                session.delete(removedItem);
                transaction.commit();
            } finally {
                session.close();
            }
        }

        return Optional.ofNullable(item);
    }

    /**
     * Gets a {@link CustomItem} by its name.
     * 
     * @param name the item name
     * @return a {@link CustomItem} or <strong>null</strong> if no item exists with the provided name.
     */
    public Optional<CustomItem> getItem(String name) {
        return Optional.ofNullable(items.get(name.toUpperCase()));
    }

    /**
     * Gets the items in this repository as an {@link ImmutableList}.
     * 
     * @param sorted states if the list is to be sorted alphabetically before returning
     * @return the items on the repository.
     */
    public List<CustomItem> asList(boolean sorted) {
        //@formatter:off
        return sorted 
                ? ImmutableList.copyOf(
                                 items.values()
                                      .stream()
                                      .sorted((item1, item2) -> {return item1.getName().compareTo(item2.getName());})
                                      .collect(toList()))
                : ImmutableList.copyOf(items.values());
        //@formatter:on
    }

    /**
     * Saves the repository to a database.
     */
    public void saveAll() {
        CustomItem currentItem = null;
        Session session = PersistenceManager.INSTANCE.getSession();
        try {
            Transaction transaction = session.beginTransaction();
            for (CustomItem item : items.values()) {
                currentItem = item;
                session.saveOrUpdate(item);
                items.put(item.getName(), item);
            }
            transaction.commit();
        } catch (Exception e) {
            Logger.error("Failed saveAll() at item: {}, with exception: {}", currentItem.toString(), e.getMessage());
            Logger.error("Items dump: {}", items);
        } finally {

            session.close();
        }
    }

    /**
     * Load items from the database.
     */
    @SuppressWarnings("unchecked")
    public void loadAll() {

        Session session = PersistenceManager.INSTANCE.getSession();

        Query query = session.createQuery("SELECT i FROM CustomItem i");
        items.clear();

        try {
            Transaction transaction = session.beginTransaction();
            List<CustomItem> list = (List<CustomItem>) query.list();
            list.forEach(i -> items.put(i.getName(), i));
            transaction.commit();
        } finally {
            session.close();
        }

        Logger.info("Loaded {} items from the database.", items.size());
    }
}
