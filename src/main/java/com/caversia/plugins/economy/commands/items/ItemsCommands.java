/**
 * 
 */
package com.caversia.plugins.economy.commands.items;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.caversia.plugins.economy.Main;
import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;

/**
 * Lists all the items related commands.
 * 
 * @author sp3c
 */
public class ItemsCommands {

    private Main plugin;

    public ItemsCommands(Main instance) {
        this.plugin = instance;
    }

    //@formatter:off
	@Command(aliases = { "create", "c" }, 
	         min = 1, 
	         usage = "<name>",
	         desc = "Creates a new custom item.", 
	         help = "Creates a new custom item based on the item in your hand.")
	//@formatter:on
    public void create(CommandContext args, CommandSender sender) throws CommandException {

        if (!sender.isOp()) {
            return;
        }

        if (!(sender instanceof Player)) {
            throw new CommandException("Only players can use this command. Please look at the commands documentation.");
        }

        new CreateItemCommand().execute(plugin, (Player) sender, args);
    }

    //@formatter:off
    @Command(aliases = { "delete", "del" }, 
             min = 1,
             usage = "<name>",
             desc = "Deletes an item from the repository.", 
             help = "Deletes an item from the repository.")
    //@formatter:on
    public void delete(CommandContext args, CommandSender sender) throws CommandException {

        if (!sender.isOp()) {
            return;
        }

        if (!(sender instanceof Player)) {
            throw new CommandException("Only players can use this command. Please look at the commands documentation.");
        }

        new DeleteItemCommand().execute(plugin, (Player) sender, args);
    }

    //@formatter:off
    @Command(aliases = { "list", "ls" }, 
             min = 0,
             max = 1,
             usage = "<page number>",
             desc = "Lists the existing items on the repository.", 
             help = "Lists the existing items on the repository using pages of 20 items each.")
    //@formatter:on
    public void list(CommandContext args, CommandSender sender) throws CommandException {

        if (!sender.isOp()) {
            return;
        }

        if (!(sender instanceof Player)) {
            throw new CommandException("Only players can use this command. Please look at the commands documentation.");
        }

        new ListItemsCommand().execute(plugin, (Player) sender, args);
    }
}
