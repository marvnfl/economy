/**
 * 
 */
package com.caversia.plugins.economy.commands.items;

import java.util.List;

import org.bukkit.entity.Player;

import com.caversia.plugins.economy.Main;
import com.caversia.plugins.economy.model.CustomItem;
import com.caversia.plugins.economy.persistence.ItemsRepository;
import com.caversia.plugins.economy.utils.ChatMessage;
import com.sk89q.minecraft.util.commands.CommandContext;

/**
 * Implementation of the list items command.
 * 
 * @author sp3c
 */
public class ListItemsCommand {

    private static final int ITEMS_PER_PAGE = 15;

    /**
     * Executes a {@link ListItemsCommand} command.
     * 
     * @param plugin this plugin instance
     * @param player the player who sent the command
     * @param args the args provided on the command
     */
    public void execute(Main plugin, Player player, CommandContext args) {
        int pageNumber = 1;
        if (args.argsLength() != 0) {
            pageNumber = args.getInteger(0);
        }

        List<CustomItem> items = ItemsRepository.INSTANCE.asList(true);
        if (items.isEmpty()) {
            ChatMessage.to(player).yellow("The are no items to be listed yet.").send();
            return;
        }

        int pagesAvailable = (items.size() / ITEMS_PER_PAGE) + 1;
        if (pageNumber < 1 || pageNumber > pagesAvailable) {
            ChatMessage.to(player).red("Invalid page number. Select a page between 1 and ", pagesAvailable).send();
            return;
        }

        printPageHeader(player, pageNumber, pagesAvailable);

        // Subtract one because all of our indexes start at 0
        pageNumber -= 1;

        int pageStart = Math.max(0, pageNumber * ITEMS_PER_PAGE);
        int pageEnd = Math.min(items.size(), pageStart + ITEMS_PER_PAGE);

        items.subList(pageStart, pageEnd).forEach(item -> ChatMessage.to(player).gold(item.getName()).send());
    }

    /**
     * Prints an items listing page header on a player chat.
     * 
     * @param player the player where the header is to be printed
     * @param pageNumber the number of the page being printed
     * @param pagesAvailable the number of pages available
     */
    private void printPageHeader(Player player, int pageNumber, int pagesAvailable) {

        //@formatter:off
        ChatMessage.to(player)
                   .bold()
                   .yellow("-------- Listing ").lPurple("Custom Items")
                   .yellow(" page ").lPurple(pageNumber).yellow(" of ").lPurple(pagesAvailable)
                   .yellow(" --------")
                   .send();
        //@formatter:on
    }
}
