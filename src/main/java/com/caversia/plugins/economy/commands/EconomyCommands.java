package com.caversia.plugins.economy.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.caversia.plugins.economy.Main;
import com.caversia.plugins.economy.commands.items.ItemsCommands;
import com.caversia.plugins.economy.utils.ChatMessage;
import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.NestedCommand;

/**
 * Lists all the Economy sub commands.
 * 
 * @author sp3c
 */
public class EconomyCommands {

    private Main plugin;

    /**
     * Default constructor.
     * 
     * @param instance this plugin instance
     */
    public EconomyCommands(Main instance) {
        this.plugin = instance;
    }

    /**
     * Root to all item related commands.
     * 
     * @param args the args received with the command
     * @param sender who sent the command
     */
    @Command(aliases = { "items" }, desc = "Caversia Economy Items root command")
    @NestedCommand(ItemsCommands.class)
    public void econItems(CommandContext args, CommandSender sender) {
        // Empty by design
    }

    /**
     * Save command.
     * 
     * @param args the args received with the command
     * @param sender who sent the command
     */
    @Command(aliases = { "save" }, desc = "Caversia Economy Save command.")
    public void saveAll(CommandContext args, CommandSender sender) {
        if (!sender.isOp()) {
            return;
        }
        plugin.saveAll();
    }

    /**
     * Reload command.
     * 
     * @param args the args received with the command
     * @param sender who sent the command
     */
    @Command(aliases = { "reload", "rl" }, desc = "Caversia Economy Reload command.")
    public void reloadAll(CommandContext args, CommandSender sender) {
        if (!sender.isOp()) {
            return;
        }

        plugin.reloadAll();
        ChatMessage.to((Player) sender).darkGreen("Caversia Economy plugin reloaded successfully!").send();
    }
}
