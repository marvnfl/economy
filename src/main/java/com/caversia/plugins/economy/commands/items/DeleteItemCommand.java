/**
 * 
 */
package com.caversia.plugins.economy.commands.items;

import java.util.Optional;

import org.bukkit.entity.Player;

import com.caversia.plugins.economy.Main;
import com.caversia.plugins.economy.model.CustomItem;
import com.caversia.plugins.economy.persistence.ItemsRepository;
import com.caversia.plugins.economy.utils.ChatMessage;
import com.caversia.plugins.economy.utils.exceptions.ItemReferencedBySignException;
import com.sk89q.minecraft.util.commands.CommandContext;

/**
 * Implementation of the delete item command.
 * 
 * @author sp3c
 */
public class DeleteItemCommand {
    /**
     * Executes a {@link DeleteItemCommand} command.
     * 
     * @param plugin this plugin instance
     * @param player the player who sent the command
     * @param args the args provided on the command
     */
    public void execute(Main plugin, Player player, CommandContext args) {

        try {
            String itemName = args.getString(0);

            Optional<CustomItem> removedItem = ItemsRepository.INSTANCE.remove(itemName);
            if (removedItem.isPresent()) {
                ChatMessage.to(player).yellow("The item ").lPurple(itemName).yellow(" was removed.").send();
                return;
            }

            ChatMessage.to(player).red("The item ").lPurple(itemName).red(" doesn't exists.").send();
        } catch (ItemReferencedBySignException srie) {
            //@formatter:off
            ChatMessage.to(player)
                       .red("You can't delete this item because it's still being referenced by buy/sell signs.")
                       .send();
            //@formatter:on
        }
    }
}
