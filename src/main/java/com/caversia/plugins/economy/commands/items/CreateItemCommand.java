/**
 * 
 */
package com.caversia.plugins.economy.commands.items;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.caversia.plugins.economy.Main;
import com.caversia.plugins.economy.model.CustomItem;
import com.caversia.plugins.economy.persistence.ItemsRepository;
import com.caversia.plugins.economy.utils.ChatMessage;
import com.caversia.plugins.economy.utils.exceptions.ItemAlreadyExistsException;
import com.sk89q.minecraft.util.commands.CommandContext;

/**
 * Implementation of the create item command.
 * 
 * @author sp3c
 */
public class CreateItemCommand {

    private static final int MAX_NAME_LENGTH = 15;

    /**
     * Executes a {@link CreateItemCommand} command.
     * 
     * @param plugin this plugin instance
     * @param player the player who sent the command
     * @param args the args provided on the command
     */
    public void execute(Main plugin, Player player, CommandContext args) {

        String itemName = args.getString(0);
        if (itemName.length() >= MAX_NAME_LENGTH) {
            ChatMessage.to(player).red("Name must have ", MAX_NAME_LENGTH, " or less characters.").send();
            return;
        }

        ItemStack item = player.getItemInHand();
        if (Material.AIR.equals(item.getType())) {
            ChatMessage.to(player).red("You need to have an item on your hand.").send();
            return;
        }

        try {
            CustomItem createdItem = new CustomItem(itemName, item);
            ItemsRepository.INSTANCE.addItem(createdItem);
            //@formatter:off
            ChatMessage.to(player)
                       .yellow("You successfully created a new item with an id of: ", createdItem.getName())
                       .send();
            //@formatter:on
        } catch (ItemAlreadyExistsException e) {
            ChatMessage.to(player).red("A item with that name already exists.").send();
        }
    }
}
