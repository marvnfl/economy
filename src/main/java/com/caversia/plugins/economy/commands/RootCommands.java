/**
 * 
 */
package com.caversia.plugins.economy.commands;

import org.bukkit.command.CommandSender;

import com.caversia.plugins.economy.Main;
import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.NestedCommand;

/**
 * Lists all the root commands that this plugin supports.
 * 
 * @author sp3c
 */
public class RootCommands {
    /**
     * Default constructor.
     * 
     * @param instance this plugin instance
     */
    public RootCommands(Main instance) {
        // Empty by design
    }

    /**
     * Root to all commands that this plugin supports.
     * 
     * @param args the args received with the command
     * @param sender who sent the command
     */
    @Command(aliases = { "econ" }, desc = "Caversia Economy root command")
    @NestedCommand(EconomyCommands.class)
    public void econItems(CommandContext args, CommandSender sender) {
        // Empty by design
    }
}
