package com.caversia.plugins.economy.utils.exceptions;

/**
 * Created by Marvin on 3/15/2015.
 */
public class EconomySetupFailedException extends Exception {
    private static final long serialVersionUID = -4439989141849767154L;

    //Empty by design.
}
