package com.caversia.plugins.economy.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.caversia.plugins.economy.model.CustomItem;
import com.caversia.plugins.economy.persistence.ItemsRepository;

/**
 * Provides convenience methods to work with an {@link Inventory}.
 * 
 * @author amartins
 */
public class InventoryUtils {

    /**
     * Counts the number of items of a given type contained on a {@link Inventory}.
     * 
     * @param inventory the {@link Inventory} to check
     * @param item the item to search on the inventory
     * @return the amount of items found
     */
    public static int countItems(Inventory inventory, ItemStack item) {

        CustomItem itemToCount = new CustomItem(null, item);

        int total = 0;
        for (ItemStack slotItem : inventory) {
            if (slotItem == null) {
                continue;
            }
            if (itemToCount.customEquals(new CustomItem(null, slotItem))) {
                total += slotItem.getAmount();
            }
        }

        return total;
    }

    /**
     * Gets the number of free slots on a inventory.
     * 
     * @param inventory the inventory to check
     * @return
     */
    public static int countNumberOfFreeSlots(Inventory inventory) {
        return Long.valueOf(Arrays.stream(inventory.getContents()).filter(item -> item != null).count()).intValue();
    }

    /**
     * Takes a given amount of items from an inventory.
     * 
     * @param inventory the inventory from which the item is to be removed
     * @param itemName the {@link CustomItem} name
     * @param amount the amount to remove
     */
    public static void takeItems(Inventory inventory, String itemName, int amount) {

        CustomItem customItem = ItemsRepository.INSTANCE.getItem(itemName).get();

        int qtyToRemove = amount;
        List<ItemStack> itemsToRemove = new ArrayList<>();
        for (ItemStack slotItem : inventory) {
            if (slotItem == null) {
                continue;
            }

            if (!customItem.customEquals(new CustomItem(null, slotItem))) {
                continue;
            }

            itemsToRemove.add(slotItem);
            qtyToRemove -= slotItem.getAmount();

            if (qtyToRemove <= 0) {
                break;
            }
        }

        itemsToRemove.forEach(itemToRemove -> inventory.removeItem(itemToRemove));

        // Because we are only able to remove stacks from the player inventory, it's possible that we remove more than
        // the amount requested. When that happens we need to restore the difference...
        if (qtyToRemove < 0) {
            inventory.addItem(customItem.asItemStack(qtyToRemove * -1));
        }
    }
}
