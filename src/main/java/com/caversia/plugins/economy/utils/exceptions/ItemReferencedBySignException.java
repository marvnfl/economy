package com.caversia.plugins.economy.utils.exceptions;

/**
 * Exception used when a player try's to delete a custom item that still is referenced by buy/sell signs.
 * 
 * @author amartins
 */
public class ItemReferencedBySignException extends Exception {

    private static final long serialVersionUID = -2979933716510038032L;
}
