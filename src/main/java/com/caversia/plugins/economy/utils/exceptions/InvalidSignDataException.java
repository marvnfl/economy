package com.caversia.plugins.economy.utils.exceptions;

/**
 * Exception used when invalid data is found on a cBuy/cSell sign.
 * 
 * @author amartins
 */
public class InvalidSignDataException extends Exception {

    private static final long serialVersionUID = 8373478062337662508L;

    /**
     * Constructor.
     * 
     * @param message the error message
     */
    public InvalidSignDataException(String message) {
        super(message);
    }
}
