/**
 * 
 */
package com.caversia.plugins.economy.utils.exceptions;

/**
 * Exception used when trying to save an item with a name that already exists on the repository.
 * 
 * @author sp3c
 */
public class ItemAlreadyExistsException extends Exception {

    private static final long serialVersionUID = -9038672519833744128L;

}
