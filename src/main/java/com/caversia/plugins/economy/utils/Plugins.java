package com.caversia.plugins.economy.utils;

import net.milkbowl.vault.economy.Economy;

/**
 * Wrapper that provides global access to 3rd party plugins.
 * 
 * @author amartins
 */
public class Plugins {
    public static Economy economy;
}
