/**
 * 
 */
package com.caversia.plugins.economy.utils;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author amartins
 */
public class Config {
    private static FileConfiguration config;
    private static JavaPlugin plugin;

    public static void initialize(JavaPlugin plugin) {
        plugin.saveDefaultConfig();

        Config.plugin = plugin;
        Config.config = plugin.getConfig();
    }

    public static void reload() {
        plugin.reloadConfig();
        Config.config = plugin.getConfig();
    }

    public static String getCurrencySymbol() {
        return config.getString("currency.symbol");
    }
}
