/**
 * 
 */
package com.caversia.plugins.economy.utils;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.block.Block;
import org.bukkit.block.Sign;

/**
 * Provides convenience methods to work with a buy/sell {@link Sign}.
 * 
 * @author amartins
 */
public class SignsUtils {

    public static final String SELL_TITLE = ChatColor.DARK_BLUE + "[cSell]";
    public static final String BUY_TITLE = ChatColor.DARK_BLUE + "[cBuy]";

    public enum Type {
        BUY, SELL;
    };

    public static float getItemPrice(String price) {
        return Float.parseFloat(price.substring(Character.isDigit(price.charAt(0)) ? 0 : 1, price.length()).trim());
    }

    public static float getItemPrice(Sign sign) {
        String price = sign.getLine(3);
        return Float.parseFloat(price.substring(Character.isDigit(price.charAt(0)) ? 0 : 1, price.length()).trim());
    }

    public static Type getSignType(Sign sign) {
        return isBuySign(sign) ? Type.BUY : Type.SELL;
    }

    public static boolean isBuySign(Block block) {
        return (block.getState() instanceof Sign) ? isBuySign((Sign) block.getState()) : false;
    }

    public static boolean isBuySign(Sign sign) {
        return sign.getLine(0).equals(BUY_TITLE);
    }

    public static boolean isSellSign(Block block) {
        return (block.getState() instanceof Sign) ? isSellSign((Sign) block.getState()) : false;
    }

    public static boolean isSellSign(Sign sign) {
        return sign.getLine(0).equals(SELL_TITLE);
    }
}
