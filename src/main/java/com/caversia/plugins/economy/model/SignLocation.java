package com.caversia.plugins.economy.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;

/**
 * Entity that represents a buy/sell sign location.
 * 
 * @author amartins
 */
@Entity
@Table(name = "items_signs_locations")
public class SignLocation {
    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "server_name", length = 40)
    private String serverName;

    @Column(name = "world_name", length = 40)
    private String worldName;

    @ManyToOne
    @JoinColumn(name = "item_id")
    private CustomItem item;

    private int x;
    private int y;
    private int z;

    /**
     * JPA Constructor.
     */
    public SignLocation() {
        super();
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Gets the id.
     *
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Constructor.
     * 
     * @param block the block used to create the identity
     */
    public SignLocation(String serverName, CustomItem item, Block block) {
        this.serverName = serverName;
        this.worldName = block.getWorld().getName();
        this.x = block.getX();
        this.y = block.getY();
        this.z = block.getZ();
        this.item = item;
    }

    /**
     * Convenience method to obtain the block {@link Location}.
     * 
     * @return the block {@link Location}.
     */
    public Location toLocation() {
        return toBlock().getLocation();
    }

    /**
     * Convenience method to obtain the {@link Block} instance that this identity represents.
     * 
     * @return the {@link Block} instance that this identity represents.
     */
    public Block toBlock() {
        return Bukkit.getWorld(worldName).getBlockAt(x, y, z);
    }

    /**
     * Gets the item.
     *
     * @return the item
     */
    public CustomItem getItem() {
        return item;
    }

    /**
     * Gets the item.
     *
     * @param item the item to set
     */
    public void setItem(CustomItem item) {
        this.item = item;
    }

    /**
     * Gets the worldName.
     *
     * @return the worldName
     */
    public String getWorldName() {
        return worldName;
    }

    /**
     * Gets the worldName.
     *
     * @param worldName the worldName to set
     */
    public void setWorldName(String worldName) {
        this.worldName = worldName;
    }

    /**
     * Gets the x.
     *
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * Gets the x.
     *
     * @param x the x to set
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Gets the y.
     *
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * Gets the y.
     *
     * @param y the y to set
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Gets the z.
     *
     * @return the z
     */
    public int getZ() {
        return z;
    }

    /**
     * Gets the z.
     *
     * @param z the z to set
     */
    public void setZ(int z) {
        this.z = z;
    }

    /**
     * Gets the serverName.
     *
     * @return the serverName
     */
    public String getServerName() {
        return serverName;
    }

    /**
     * Sets the serverName.
     *
     * @param serverName the serverName to set
     */
    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((serverName == null) ? 0 : serverName.hashCode());
        result = prime * result + ((worldName == null) ? 0 : worldName.hashCode());
        result = prime * result + x;
        result = prime * result + y;
        result = prime * result + z;
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SignLocation other = (SignLocation) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (serverName == null) {
            if (other.serverName != null)
                return false;
        } else if (!serverName.equals(other.serverName))
            return false;
        if (worldName == null) {
            if (other.worldName != null)
                return false;
        } else if (!worldName.equals(other.worldName))
            return false;
        if (x != other.x)
            return false;
        if (y != other.y)
            return false;
        if (z != other.z)
            return false;
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SignLocation [id=" + id + ", serverName=" + serverName + ", worldName=" + worldName + ", item=" + item
                + ", x=" + x + ", y=" + y + ", z=" + z + "]";
    }
}
