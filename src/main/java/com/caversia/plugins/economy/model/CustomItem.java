/**
 * 
 */
package com.caversia.plugins.economy.model;

import static java.util.stream.Collectors.toList;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.caversia.plugins.economy.persistence.ItemsRepository;
import com.caversia.plugins.economy.utils.Logger;
import com.google.common.collect.Lists;

/**
 * Represents a Caversia custom item.
 * 
 * @author sp3c
 */
@Entity
@Table(name = "custom_items")
public class CustomItem {
    @Id
    private String name;

    @Column(name = "material")
    private Material material;

    @Column(name = "display_name")
    private String displayName;

    @Embedded
    private MNSInfo mnsInfo;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "custom_items_lore", joinColumns = { @JoinColumn(name = "item_name") })
    private List<String> lore;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "custom_items_enchantments", joinColumns = { @JoinColumn(name = "item_name") })
    private List<String> enchantments;

    @OneToMany(mappedBy = "item", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<SignLocation> signs;

    /**
     * JPA Constructor.
     */
    public CustomItem() {
        super();
    }

    /**
     * Constructor.
     * 
     * @param name the name/id of this item
     * @param item the minecraft item
     */
    public CustomItem(String name, ItemStack item) {
        if (name != null) {
            this.name = name.toUpperCase();
        }

        this.material = item.getType();
        this.mnsInfo = new MNSInfo(item);

        ItemMeta itemMeta = item.getItemMeta();
        this.lore = itemMeta.getLore();
        this.displayName = itemMeta.getDisplayName();
        this.enchantments = itemMeta.getEnchants().keySet().stream().map(Enchantment::getName).collect(toList());
    }

    /**
     * Creates a new {@link ItemStack} from this custom item.
     * 
     * @param amount the amount of instances from this item to return
     * @return a stack with the amount of requested items.
     */
    @Transient
    public ItemStack asItemStack(int amount) {
        ItemStack item = new ItemStack(material, amount);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setLore(lore);
        itemMeta.setDisplayName(displayName);
        for (String enchantmentName : enchantments) {
            itemMeta.addEnchant(Enchantment.getByName(enchantmentName), 1, true);
        }

        item.setItemMeta(itemMeta);

        return mnsInfo.injectInto(item);
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the name.
     *
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the material.
     *
     * @return the material
     */
    public Material getMaterial() {
        return material;
    }

    /**
     * Gets the material.
     *
     * @param material the material to set
     */
    public void setMaterial(Material material) {
        this.material = material;
    }

    /**
     * Gets the displayName.
     *
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Gets the displayName.
     *
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Gets the lore.
     *
     * @return the lore
     */
    public List<String> getLore() {
        return lore;
    }

    /**
     * Gets the lore.
     *
     * @param lore the lore to set
     */
    public void setLore(List<String> lore) {
        this.lore = lore;
    }

    /**
     * Gets the enchantments.
     *
     * @return the enchantments
     */
    public List<String> getEnchantments() {
        return enchantments;
    }

    /**
     * Gets the enchantments.
     *
     * @param enchantments the enchantments to set
     */
    public void setEnchantments(List<String> enchantments) {
        this.enchantments = enchantments;
    }

    /**
     * Adds a new sign relation to this item.
     * 
     * @param block the block where the sign is located
     * @param serverName the current server name
     */
    public void addSignReference(String serverName, Block block) {
        if (signs == null) {
            signs = new HashSet<>();
        }
        signs.add(new SignLocation(serverName, this, block));
        ItemsRepository.INSTANCE.setChanged();

        Logger.trace("Added a sign reference from custom item {}", name);
    }

    /**
     * Removes a sign relation from this item.
     * 
     * @param block the block where the sign was located
     * @param serverName the current server name
     */
    public void removeSignReference(String serverName, Block block) {
        if (signs == null) {
            return;
        }
        signs.remove(new SignLocation(serverName, this, block));
        ItemsRepository.INSTANCE.setChanged();

        Logger.trace("Removed a sign reference from custom item {}", name);
    }

    /**
     * Gets if this item is being referenced by buy/sell signs.
     * 
     * @return <code>true</code> if the item has references, <code>false</code> otherwise.
     */
    public boolean isReferencedBySigns() {
        return signs != null && !signs.isEmpty();
    }

    /**
     * Gets the signs.
     *
     * @return the signs
     */
    public Set<SignLocation> getSigns() {
        return signs;
    }

    /**
     * Gets the signs.
     *
     * @param signs the signs to set
     */
    public void setSigns(Set<SignLocation> signs) {
        this.signs = signs;
    }

    public boolean customEquals(CustomItem item) {
        if (displayName == null) {
            if (item.displayName != null) {
                return false;
            }
        } else if (!displayName.equals(item.displayName)) {
            return false;
        }

        if (!material.equals(item.material)) {
            return false;
        }

        if (enchantments == null) {
            if (item.enchantments != null) {
                return false;
            }
        }

        if (!Lists.newCopyOnWriteArrayList(enchantments).equals(item.enchantments)) {
            return false;
        }

        if (lore == null || lore.isEmpty()) {
            if (item.lore != null) {
                return false;
            }
        } else if (!Lists.newCopyOnWriteArrayList(lore).equals(item.lore)) {
            return false;
        }

        return mnsInfo.equals(item.mnsInfo);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CustomItem other = (CustomItem) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CustomItem [name=" + name + ", material=" + material + ", displayName=" + displayName + ", mnsInfo="
                + mnsInfo + ", lore=" + lore + ", enchantments=" + enchantments + ", signs=" + signs + "]";
    }
}