/**
 * 
 */
package com.caversia.plugins.economy.model;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import net.minecraft.server.v1_8_R2.NBTReadLimiter;
import net.minecraft.server.v1_8_R2.NBTTagCompound;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.bukkit.craftbukkit.v1_8_R2.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import com.caversia.plugins.economy.utils.Logger;
import com.google.common.base.Strings;

/**
 * @author amartins
 */
@Embeddable
public class MNSInfo {

    @Column(name = "data")
    private Integer data;

    @Column(name = "nbt", columnDefinition = "TEXT")
    private String nbt;

    /**
     * JPA Constructor.
     */
    public MNSInfo() {
        super();
    }

    public MNSInfo(ItemStack item) {
        net.minecraft.server.v1_8_R2.ItemStack mnsItem = CraftItemStack.asNMSCopy(item);
        data = Integer.valueOf(mnsItem.getData());

        if (mnsItem.getTag() != null) {
            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            OutputStream bos = new BufferedOutputStream(bao);
            DataOutputStream dos = new DataOutputStream(bos);
            try {
                Method method = NBTTagCompound.class.getDeclaredMethod("write", DataOutput.class);
                method.setAccessible(true);
                method.invoke(mnsItem.getTag(), dos);
                dos.flush();

                nbt = Strings.emptyToNull(String.valueOf(Hex.encodeHex(bao.toByteArray())));
            } catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchMethodException
                    | InvocationTargetException | IOException e) {
                throw new RuntimeException("Failed to extract MNS info from item!", e);
            } finally {
                try {
                    dos.close();
                    bos.close();
                    bao.close();
                } catch (IOException e) {
                    Logger.error("Failed to close buffers: {}", e.getMessage());
                }
            }
        }
    }

    public ItemStack injectInto(ItemStack item) {
        net.minecraft.server.v1_8_R2.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
        nmsItem.setData(data);
        if (nbt != null) {

            byte[] nbtBytes;
            try {
                nbtBytes = Hex.decodeHex(nbt.toCharArray());
            } catch (DecoderException e) {
                throw new RuntimeException("Failed to inject NMS info into an item!", e);
            }

            ByteArrayInputStream bai = new ByteArrayInputStream(nbtBytes);
            InputStream is = new BufferedInputStream(bai);
            DataInputStream dis = new DataInputStream(is);
            try {
                Method method = NBTTagCompound.class.getDeclaredMethod("load", DataInput.class, int.class,
                        NBTReadLimiter.class);
                method.setAccessible(true);
                NBTTagCompound tagCompound = new NBTTagCompound();
                method.invoke(tagCompound, dis, 0, NBTReadLimiter.a);
                nmsItem.setTag(tagCompound);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
                    | NoSuchMethodException | SecurityException e) {
                throw new RuntimeException("Failed to inject NMS info into an item!", e);
            } finally {
                try {
                    dis.close();
                    is.close();
                    bai.close();
                } catch (IOException e) {
                    Logger.error("Failed to close buffers: {}", e.getMessage());
                }
            }
        }

        return CraftItemStack.asBukkitCopy(nmsItem);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((data == null) ? 0 : data.hashCode());
        result = prime * result + ((nbt == null) ? 0 : nbt.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MNSInfo other = (MNSInfo) obj;
        if (data == null) {
            if (other.data != null)
                return false;
        } else if (!data.equals(other.data))
            return false;
        if (nbt == null) {
            if (other.nbt != null)
                return false;
        } else if (!nbt.equals(other.nbt))
            return false;
        return true;
    }
}
